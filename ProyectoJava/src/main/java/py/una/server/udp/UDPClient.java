package py.una.server.udp;

import com.google.gson.Gson;
import java.io.*;
import java.net.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import py.una.entidad.Sensor;

class UDPClient {
    
    private static final Logger logger = LogManager.getLogger(UDPClient.class);

    public static void main(String a[]) throws Exception {
        
        
        logger.info("Rodrillo Amarilla Sanabria - 20/06/1987");
        System.out.println("Rodrillo Amarilla Sanabria - 20/06/1987");

        Gson gson = new Gson();
        // Datos necesario
        String direccionServidor = "127.0.0.1";

        if (a.length > 0) {
            direccionServidor = a[0];
        }

        int puertoServidor = 9876;

        try {

            BufferedReader inFromUser
                    = new BufferedReader(new InputStreamReader(System.in));

            DatagramSocket clientSocket = new DatagramSocket();

            InetAddress IPAddress = InetAddress.getByName(direccionServidor);
            System.out.println("Intentando conectar a = " + IPAddress + ":" + puertoServidor + " via UDP...");

            byte[] sendData = new byte[1024];
            byte[] receiveData = new byte[1024];

            System.out.print("Ingrese la operacion: ");
            Integer operacion = Integer.valueOf(inFromUser.readLine());

            Sensor s = new Sensor();
            if (operacion == 1) {
                System.out.print("Ingrese el idEstacion: ");
                Integer idEstacion = Integer.valueOf(inFromUser.readLine());
                System.out.print("Ingrese ciudad: ");
                String ciudad = inFromUser.readLine();
                System.out.print("Ingrese el porcentajeHumedad: ");
                Integer porcentajeHumedad = Integer.valueOf(inFromUser.readLine());
                System.out.print("Ingrese la temperatura: ");
                Integer temperatura = Integer.valueOf(inFromUser.readLine());
                System.out.print("Ingrese la velocidad del Viento: ");
                Integer velocidadViento = Integer.valueOf(inFromUser.readLine());
                System.out.print("Ingrese la fecha: ");
                String fecha = inFromUser.readLine();
                System.out.print("Ingrese la hora: ");
                String hora = inFromUser.readLine();
                
                s = new Sensor(idEstacion, ciudad, porcentajeHumedad, temperatura, velocidadViento, fecha, hora, operacion);
                
                //cargar datos 
            } else {
                System.out.print("Ingrese ciudad: ");
                String ciudad = inFromUser.readLine();
                
                //cargar datos
                
                s = new Sensor();
                
                s.setCiudad(ciudad);
                s.setOperacion(operacion);
            }

            

            String datoPaquete = gson.toJson(s);
            sendData = datoPaquete.getBytes();

            System.out.println("Enviar " + datoPaquete + " al servidor. (" + sendData.length + " bytes)");
            DatagramPacket sendPacket
                    = new DatagramPacket(sendData, sendData.length, IPAddress, puertoServidor);

            clientSocket.send(sendPacket);

            DatagramPacket receivePacket
                    = new DatagramPacket(receiveData, receiveData.length);

            System.out.println("Esperamos si viene la respuesta.");

            //Vamos a hacer una llamada BLOQUEANTE entonces establecemos un timeout maximo de espera
            clientSocket.setSoTimeout(10000);

            try {
                // ESPERAMOS LA RESPUESTA, BLOQUENTE
                clientSocket.receive(receivePacket);

                String respuesta = new String(receivePacket.getData());

                Sensor presp = gson.fromJson(respuesta.trim(), Sensor.class);
                
                System.out.println(presp.toString());

                InetAddress returnIPAddress = receivePacket.getAddress();
                int port = receivePacket.getPort();

            } catch (SocketTimeoutException ste) {

                System.out.println("TimeOut: El paquete udp se asume perdido.");
            }
            clientSocket.close();
        } catch (UnknownHostException ex) {
            System.err.println(ex);
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }
}
