package py.una.server.udp;


import java.net.*;

import com.google.gson.Gson;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import py.una.entidad.Sensor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;




public class UDPServer {
    
    private static final Logger logger = LogManager.getLogger(UDPServer.class);
	
	
    public static void main(String[] a){
        
        // Variables
        int puertoServidor = 9876;

        
        try {
            
            
            logger.info("Rodrillo Amarilla Sanabria - 20/06/1987");
            System.out.println("Rodrillo Amarilla Sanabria - 20/06/1987");
            //1) Creamos el socket Servidor de Datagramas (UDP)
            DatagramSocket serverSocket = new DatagramSocket(puertoServidor);
			System.out.println("Servidor Sistemas Distribuidos - UDP ");
			
            //2) buffer de datos a enviar y recibir
            byte[] receiveData = new byte[1024];
            byte[] sendData = new byte[1024];
            
            List<Sensor> lista = new ArrayList();

			
            //3) Servidor siempre esperando
            while (true) {

                receiveData = new byte[1024];

                DatagramPacket receivePacket =
                        new DatagramPacket(receiveData, receiveData.length);


                System.out.println("Esperando a algun cliente... ");

                // 4) Receive LLAMADA BLOQUEANTE
                serverSocket.receive(receivePacket);
				
				System.out.println("________________________________________________");
                System.out.println("Aceptamos un paquete");

                // Datos recibidos e Identificamos quien nos envio
                String datoRecibido = new String(receivePacket.getData());
                datoRecibido = datoRecibido.trim();
                System.out.println("DatoRecibido: " + datoRecibido );
                
                Gson gson = new Gson();

                
                
                Sensor s = gson.fromJson(datoRecibido, Sensor.class);
                
                
                Sensor respuesta = new Sensor();
                if(s.getOperacion().equals(1)){
                    lista.add(s);
                    respuesta = s;
                } else if(s.getOperacion().equals(2)){
                    for (Sensor dato : lista) {
                        if(dato.getCiudad().equals(s.getCiudad())){
                            System.out.println("encontrado");
                            System.out.println(dato.toString());
                            respuesta = dato;
                        }
                    }
                }
                
                
       

                InetAddress IPAddress = receivePacket.getAddress();

                int port = receivePacket.getPort();

                System.out.println("De : " + IPAddress + ":" + port);
                System.out.println("Dato Recibido : " + s.toString());
                
                try {
                	
                    // Agregar guardar objet en un array list
                	
                }catch(Exception e) {

                        e.printStackTrace();
                                
                }
                
               
                sendData = gson.toJson(respuesta).getBytes();
                DatagramPacket sendPacket =
                        new DatagramPacket(sendData, sendData.length, IPAddress,port);

                serverSocket.send(sendPacket);

            }

        } catch (Exception ex) {
        	ex.printStackTrace();
            System.exit(1);
        }

    }
}  

