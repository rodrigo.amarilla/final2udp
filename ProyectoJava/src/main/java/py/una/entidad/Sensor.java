
package py.una.entidad;

import java.util.Date;


public class Sensor {
    
    Integer idEstacion;
    String ciudad;
    Integer porcentajeHumedad;
    Integer temperatura;
    Integer velocidadViento;
    String fecha;
    String hora;
    Integer operacion;

    public Sensor(Integer idEstacion, String ciudad, Integer porcentajeHumedad, Integer temperatura, Integer velocidadViento, String fecha, String hora, Integer operacion) {
        this.idEstacion = idEstacion;
        this.ciudad = ciudad;
        this.porcentajeHumedad = porcentajeHumedad;
        this.temperatura = temperatura;
        this.velocidadViento = velocidadViento;
        this.fecha = fecha;
        this.hora = hora;
        this.operacion = operacion;
    }

    
    public Sensor() {
    }

    
    
    
    

    public Integer getIdEstacion() {
        return idEstacion;
    }

    public void setIdEstacion(Integer idEstacion) {
        this.idEstacion = idEstacion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public Integer getPorcentajeHumedad() {
        return porcentajeHumedad;
    }

    public void setPorcentajeHumedad(Integer porcentajeHumedad) {
        this.porcentajeHumedad = porcentajeHumedad;
    }

    public Integer getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(Integer temperatura) {
        this.temperatura = temperatura;
    }

    public Integer getVelocidadViento() {
        return velocidadViento;
    }

    public void setVelocidadViento(Integer velocidadViento) {
        this.velocidadViento = velocidadViento;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

   

    public Integer getOperacion() {
        return operacion;
    }

    public void setOperacion(Integer operacion) {
        this.operacion = operacion;
    }

    @Override
    public String toString() {
        return "Sensor{" + "idEstacion=" + idEstacion + ", ciudad=" + ciudad + ", porcentajeHumedad=" + porcentajeHumedad + ", temperatura=" + temperatura + ", velocidadViento=" + velocidadViento + ", fecha=" + fecha + ", operacion=" + operacion + '}';
    }
    
    

   
    
}
